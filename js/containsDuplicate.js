// O(n)
const containsDuplicate = function (nums) {
  const numsSet = new Set(nums);
  return numsSet.size !== nums.length;
};

console.log(containsDuplicate([1, 3, 2, 5, 1]))