var twoSum = function (nums, target) {

  const map = new Map();

  for (let i = 0; i < nums.length; i++) { // O(n)
    const num = nums[i];
    const requiredNo = target - num;

    if (map.has(requiredNo)) {
      return [map.get(requiredNo), i]
    }

    map.set(num, i)
  }
};


console.log(twoSum([2, 11, 15, 7], 9))