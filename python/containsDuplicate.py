def containsDuplicate(nums: list[int]) -> bool:
  # List to set time complexity: O(n)
  setFromList = set(nums)
  # Check length time complexity: O(1)
  if(len(nums) == len(setFromList)):
    return False
  else:
    return True


# Test Cases
nums = [
  [1,2,3,1],
  [1,2,3,4],
  [1,1,1,3,3,4,3,2,4,2]
]

for numsList in nums:
  print(containsDuplicate(numsList))